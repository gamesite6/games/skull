import path from "path";
import { defineConfig } from "vite";
import elmPlugin from "vite-plugin-elm";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/death-and-taxes/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: path.resolve(__dirname, "src", "main.ts"),
      fileName: "death-and-taxes",
      name: "Gamesite6_DeathAndTaxes",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte(), elmPlugin()],
});
