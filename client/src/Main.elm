port module Main exposing (main)

import Browser
import Css exposing (..)
import Css.Transitions as Transitions exposing (transition)
import Gen.Server exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events exposing (onClick)
import Json.Decode as D
import Json.Encode as E
import List.Extra exposing (splitWhen)


main : Program E.Value Model Msg
main =
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view >> toUnstyled
        }


type alias Model =
    Result
        String
        { props : Props
        , inputs : Inputs
        }


type alias Inputs =
    { bid : String
    }


initialInputs : Inputs
initialInputs =
    { bid = "" }


type alias Props =
    { playerId : Maybe PlayerId
    , state : State
    , settings : Settings
    }


jsonDecProps : D.Decoder Props
jsonDecProps =
    D.map3 Props
        (D.field "playerId" (D.maybe D.int))
        (D.field "state" jsonDecState)
        (D.field "settings" jsonDecSettings)


type alias PlayerId =
    Int


init : E.Value -> ( Model, Cmd Msg )
init initialStateJson =
    case D.decodeValue jsonDecProps initialStateJson of
        Ok props ->
            ( Ok
                { props = props
                , inputs = initialInputs
                }
            , Cmd.none
            )

        Err err ->
            ( Err (D.errorToString err), Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    setProps PropsChanged


port onAction : E.Value -> Cmd msg


port setProps : (E.Value -> msg) -> Sub msg


type Msg
    = PerformAction Action
    | PropsChanged E.Value
    | ChangeBidInput String
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg modelRes =
    case ( msg, modelRes ) of
        ( PerformAction action, Ok model ) ->
            let
                inputs =
                    model.inputs
            in
            ( Ok { model | inputs = { inputs | bid = "" } }, onAction (jsonEncAction action) )

        ( PropsChanged propsJson, _ ) ->
            case D.decodeValue jsonDecProps propsJson of
                Ok nextProps ->
                    let
                        inputs =
                            case modelRes of
                                Ok model ->
                                    model.inputs

                                Err _ ->
                                    initialInputs
                    in
                    ( Ok
                        { props = nextProps
                        , inputs = inputs
                        }
                    , Cmd.none
                    )

                Err err ->
                    ( Err (D.errorToString err), Cmd.none )

        ( ChangeBidInput value, Ok model ) ->
            let
                inputs =
                    model.inputs
            in
            ( Ok { model | inputs = { inputs | bid = value } }, Cmd.none )

        ( NoOp, _ ) ->
            ( modelRes, Cmd.none )

        ( _, Err err ) ->
            ( Err err, Cmd.none )


view : Model -> Html Msg
view model =
    case model of
        Err err ->
            code [] [ text err ]

        Ok { props, inputs } ->
            let
                { settings, state } =
                    props

                userPlayerId =
                    props.playerId

                _ =
                    settings

                orderedPlayers =
                    orderPlayers userPlayerId state.players
            in
            div
                [ css
                    [ position relative
                    , height (pct 100)
                    ]
                ]
                (List.concat
                    [ orderedPlayers
                        |> List.indexedMap (playerView userPlayerId inputs state)
                    , [ div
                            [ css
                                [ position absolute
                                , top (pct 50)
                                , left (pct 50)
                                , transform (translate2 (pct -50) (pct -50))
                                , textAlign center
                                , maxWidth (Css.em 12)
                                ]
                            ]
                            (case state.phase of
                                Prepare _ ->
                                    [ h2 [] [ text "Preparation" ]
                                    , p [] [ text "Everyone must play a card." ]
                                    ]

                                AddOrChallenge playerId ->
                                    [ p []
                                        [ gs6UserView playerId ""
                                        , text " must play a card or place a bid."
                                        ]
                                    ]

                                BidOrPass { playerId, highestBid, highestBidder } ->
                                    [ h3 [] [ text "Highest bid" ]
                                    , p []
                                        [ strong [ css [ fontSize (Css.em 2.5) ] ] [ text (String.fromInt highestBid) ]
                                        , br [] []
                                        , gs6UserView highestBidder ""
                                        ]
                                    , hr [] []
                                    , p []
                                        [ gs6UserView playerId ""
                                        , text " must raise the bid or pass."
                                        ]
                                    ]

                                FlipOwn { bid, playerId } ->
                                    [ h3 [] [ text "Winning bid" ]
                                    , h3 [] [ strong [ css [ fontSize (Css.em 2.5) ] ] [ text (String.fromInt bid) ] ]
                                    , div [] [ gs6UserView playerId "", text " must flip all their cards." ]
                                    ]

                                Attempt { playerId, goal } ->
                                    [ div []
                                        [ h3 [] [ text "Attempt in progress" ]
                                        , p []
                                            [ strong [ css [ fontSize (Css.em 2.5) ] ]
                                                [ text (String.fromInt (flippedMoneys state.players))
                                                , text "\u{2009}/\u{2009}"
                                                , text (String.fromInt goal)
                                                ]
                                            ]
                                        , p [] [ gs6UserView playerId "" ]
                                        ]
                                    ]

                                Success playerId ->
                                    [ h2 [] [ text "Success" ]
                                    , p []
                                        [ gs6UserView playerId ""
                                        , text " scores a point!"
                                        ]
                                    , p []
                                        (case List.Extra.find (\p -> p.id == playerId) state.players of
                                            Just player ->
                                                [ span [ css [ fontSize (Css.em 2) ] ]
                                                    [ text (String.fromInt player.score)
                                                    , text "\u{2009}⟶\u{2009}"
                                                    , strong [] [ text (String.fromInt (player.score + 1)) ]
                                                    ]
                                                ]

                                            Nothing ->
                                                []
                                        )
                                    , if userPlayerId == Just playerId then
                                        button
                                            [ Attr.class "primary"
                                            , onClick (PerformAction SuccessOk)
                                            ]
                                            [ text "Ok" ]

                                      else
                                        text ""
                                    ]

                                DiscardTarget { playerId, targetId } ->
                                    [ h2 [] [ text "Failed" ]
                                    , p
                                        []
                                        [ gs6UserView targetId ""
                                        , text " flipped "
                                        , gs6UserView playerId "'s"
                                        , text " skull."
                                        ]
                                    , p []
                                        [ gs6UserView playerId ""
                                        , text " will discard one of "
                                        , gs6UserView targetId "'s"
                                        , text " cards!"
                                        ]
                                    ]

                                DiscardSelf playerId ->
                                    [ h2 [] [ text "Failed" ]
                                    , p []
                                        [ gs6UserView playerId ""
                                        , text " flipped their own skull and must discard one of their cards."
                                        ]
                                    ]

                                GameComplete ->
                                    let
                                        maybeWinner =
                                            state.players
                                                |> find (\p -> p.score == 2)
                                    in
                                    case maybeWinner of
                                        Just winner ->
                                            [ h2 []
                                                [ node "gs6-user" [ Attr.property "playerid" (E.int winner.id) ] []
                                                , text " wins!"
                                                ]
                                            ]

                                        Nothing ->
                                            []
                            )
                      ]
                    ]
                )


find : (a -> Bool) -> List a -> Maybe a
find pred xs =
    case xs of
        h :: t ->
            if pred h then
                Just h

            else
                find pred t

        [] ->
            Nothing


gs6UserView : PlayerId -> String -> Html msg
gs6UserView playerId suffix =
    node "gs6-user" [ Attr.attribute "playerid" (String.fromInt playerId), Attr.attribute "suffix" suffix ] []


flippedMoneys : List Player -> Int
flippedMoneys =
    List.sum << List.map (List.Extra.count ((==) Money) << .faceUp)


isActive : Phase -> PlayerId -> Bool
isActive phase pid =
    case phase of
        Prepare { active } ->
            List.member pid active

        AddOrChallenge playerId ->
            playerId == pid

        BidOrPass { playerId } ->
            playerId == pid

        FlipOwn { playerId } ->
            playerId == pid

        Attempt { playerId } ->
            playerId == pid

        DiscardSelf playerId ->
            playerId == pid

        DiscardTarget { playerId } ->
            playerId == pid

        Success playerId ->
            playerId == pid

        GameComplete ->
            False


canAddCard : Phase -> PlayerId -> Bool
canAddCard phase playerId =
    case phase of
        Prepare { active } ->
            List.member playerId active

        AddOrChallenge activeId ->
            activeId == playerId

        _ ->
            False


canBid : Phase -> PlayerId -> Bool
canBid phase playerId =
    case phase of
        AddOrChallenge activeId ->
            playerId == activeId

        BidOrPass ({ highestBidder, passed } as bidOrPass) ->
            (bidOrPass.playerId == playerId)
                && (playerId /= highestBidder)
                && not (List.member playerId passed)

        _ ->
            False


canPass : Phase -> PlayerId -> Bool
canPass phase playerId =
    case phase of
        BidOrPass ({ highestBidder, passed } as bidOrPass) ->
            (bidOrPass.playerId == playerId)
                && (playerId /= highestBidder)
                && not (List.member playerId passed)

        _ ->
            False


canDiscardOwn : Phase -> PlayerId -> Bool
canDiscardOwn phase playerId =
    case phase of
        DiscardSelf activePlayerId ->
            playerId == activePlayerId

        _ ->
            False


type CardFace
    = FaceUp
    | FaceDown


cardView : Index -> CardFace -> Card -> Html Msg
cardView _ cardFace card =
    div
        [ Attr.class "border"
        , Attr.class "shadow"
        , css
            [ backgroundColor linen
            , boxSizing borderBox
            , height (Css.em 2)
            , width (Css.em 2)
            , borderRadius (pct 100)
            , position relative
            ]
        ]
        (case ( cardFace, card ) of
            ( FaceUp, Money ) ->
                [ div
                    [ Attr.class "emoji"
                    , css
                        [ position absolute
                        , top (pct 50)
                        , left (pct 50)
                        , transforms [ translate2 (pct -50) (pct -50), scale 1.05 ]
                        , lineHeight (pct 0)
                        , Css.property "filter" "drop-shadow(0 0 0.05em #000a)"
                        ]
                    ]
                    [ text "💰" ]
                ]

            ( FaceUp, Skull ) ->
                [ div
                    [ Attr.class "emoji"
                    , css
                        [ position absolute
                        , top (pct 50)
                        , left (pct 50)
                        , transforms [ translate2 (pct -50) (pct -50), scale 1.15 ]
                        , lineHeight (pct 0)
                        , Css.property "filter" "drop-shadow(0 0 0.05em #000a)"
                        ]
                    ]
                    [ text "☠️" ]
                ]

            ( FaceDown, _ ) ->
                [ div
                    [ css
                        [ position absolute
                        , top (pct 12.5)
                        , bottom (pct 12.5)
                        , left (pct 12.5)
                        , right (pct 12.5)
                        , borderRadius (pct 100)
                        , property "background" """
                            repeating-linear-gradient(
                                45deg,
                                #00000010,
                                #00000010 5%,
                                #00000020 5%,
                                #00000020 10%
                            ),
                            repeating-linear-gradient(
                                -45deg,
                                #00000010,
                                #00000010 5%,
                                #00000020 5%,
                                #00000020 10%
                            ),
                            radial-gradient(hsl(250, 25%, 60%), hsl(250, 40%, 40%));
                        """
                        ]
                    ]
                    []
                ]
        )


linen : Color
linen =
    rgb 250 240 230


type alias Index =
    Int


cardButton : Index -> List (Attribute Msg) -> CardFace -> Card -> Html Msg
cardButton playerIndex attrs cardFace card =
    button
        ([ Attr.type_ "button"
         , Attr.class "invisibutton"
         , css
            [ display block
            , textAlign center
            , fontSize inherit
            , transition
                [ Transitions.transform 250
                ]
            , lineHeight inherit
            , border (px 0)
            , padding (px 0)
            , backgroundColor inherit
            , pseudoClass "not(:disabled)"
                [ hover
                    [ transforms
                        [ translateY (pct -20)
                        ]
                    , cursor pointer
                    ]
                , focus
                    [ transforms
                        [ translateY (pct -20)
                        ]
                    ]
                ]
            ]
         ]
            ++ attrs
        )
        [ cardView playerIndex cardFace card ]


playerView : Maybe PlayerId -> Inputs -> State -> Index -> Player -> Html Msg
playerView maybePlayerId inputs state i player =
    let
        isUser =
            maybePlayerId == Just player.id

        isActivePlayer =
            isActive state.phase player.id

        playerCanBid : Bool
        playerCanBid =
            canBid state.phase player.id

        rotation : Float
        rotation =
            toFloat i * (2 * pi) / toFloat (List.length state.players)

        xPos =
            sin rotation * -(16 + playerCount)

        yPos =
            cos rotation * (13 + playerCount)

        playerCount =
            toFloat (List.length state.players)

        speech : Maybe String
        speech =
            case state.phase of
                BidOrPass { passed } ->
                    if List.member player.id passed then
                        Just "I pass!"

                    else
                        Nothing

                _ ->
                    Nothing
    in
    div
        [ css
            [ position absolute
            , left (pct 50)
            , top (pct 50)
            , transforms
                [ translateX (pct -50)
                , translateY (pct -50)
                , translate2 (rem xPos) (rem yPos)
                ]
            ]
        ]
        [ handView maybePlayerId state i player
        , node "gs6-player-box"
            [ Attr.property "playerid" (E.int player.id)
            , Attr.attribute "speaking"
                (case speech of
                    Just _ ->
                        "true"

                    Nothing ->
                        ""
                )
            , Attr.attribute "thinking"
                (if isActivePlayer then
                    "true"

                 else
                    ""
                )
            ]
            [ div [ Attr.property "slot" (E.string "speech") ] [ text (Maybe.withDefault "" speech) ]
            , div
                [ css [ Css.textAlign Css.center ] ]
                [ text "Score: ", text (String.fromInt player.score) ]
            , if isUser && playerCanBid then
                let
                    highestBid =
                        case state.phase of
                            BidOrPass p ->
                                p.highestBid

                            _ ->
                                0

                    totalPlayedCards =
                        state.players
                            |> List.map (.faceDown >> List.length)
                            |> List.sum

                    handleSubmit =
                        case String.toInt inputs.bid of
                            Just bid ->
                                PerformAction (Bid bid)

                            Nothing ->
                                NoOp
                in
                div
                    [ css
                        [ position absolute
                        , top (pct 0)
                        , right (pct 0)
                        , transforms [ translateX (pct 100) ]
                        , paddingLeft (Css.em 1)
                        ]
                    ]
                    [ div
                        [ css
                            [ Css.property "display" "grid"
                            , Css.property "column-gap" "0.25em"
                            , Css.property "grid-template-columns" "auto auto"
                            ]
                        ]
                        [ label
                            [ Attr.for "user-bid"
                            , css [ Css.property "grid-column" "1 / 3" ]
                            ]
                            [ text "Cards to flip" ]
                        , let
                            minBid =
                                highestBid + 1

                            maxBid =
                                totalPlayedCards
                          in
                          select
                            [ Attr.id "user-bid"
                            , Events.onInput ChangeBidInput
                            , Attr.disabled (minBid > maxBid)
                            , css
                                [ Css.width (Css.rem 3)
                                , Css.padding Css.zero
                                , Css.borderRadius (Css.rem 0.25)
                                , Css.property "background" "white"
                                , Css.borderColor (Css.hex "0007")
                                , Css.textAlign Css.center
                                ]
                            ]
                            (option
                                [ Attr.disabled True, Attr.hidden True, Attr.selected (inputs.bid == "") ]
                                []
                                :: (List.range minBid maxBid
                                        |> List.reverse
                                        |> List.map
                                            (\bid ->
                                                let
                                                    value =
                                                        String.fromInt bid
                                                in
                                                option
                                                    [ Attr.value value
                                                    , Attr.selected (inputs.bid == value)
                                                    ]
                                                    [ text value ]
                                            )
                                   )
                            )
                        , text " "
                        , button
                            [ css
                                []
                            , Attr.class "primary"
                            , Attr.property "primary" (E.string "")
                            , Attr.property "disabled" (E.bool (inputs.bid == ""))
                            , Events.onClick handleSubmit
                            ]
                            [ text "Bid" ]
                        ]
                    , if canPass state.phase player.id then
                        div
                            [ css
                                []
                            ]
                            [ div
                                [ css
                                    [ Css.before [ Css.property "content" "\" ~ \"" ]
                                    , Css.after [ Css.property "content" "\" ~ \"" ]
                                    , marginTop (Css.em 0.5)
                                    , marginBottom (Css.em 0.5)
                                    ]
                                ]
                                [ text "or" ]
                            , button
                                [ Attr.class "secondary"
                                , css
                                    [ Css.height (Css.rem 2.5)
                                    ]
                                , onClick (PerformAction Pass)
                                ]
                                [ text "Pass" ]
                            ]

                      else
                        text ""
                    ]

              else
                text ""
            , div
                [ css
                    [ position absolute
                    , left (pct 50)
                    , transform (translateX (pct -50))
                    , displayFlex
                    , justifyContent spaceBetween
                    , fontSize (Css.em 2)
                    ]
                ]
                (let
                    ( faceDown, faceUp ) =
                        case state.phase of
                            DiscardTarget { targetId, playerId } ->
                                let
                                    ( moneys, skulls ) =
                                        player.hand
                                in
                                if player.id == targetId then
                                    ( (List.range 1 skulls |> List.map (\_ -> Skull))
                                        |> List.append (List.range 1 moneys |> List.map (\_ -> Money))
                                        |> List.append player.faceUp
                                        |> List.append player.faceDown
                                    , []
                                    )

                                else
                                    ( player.faceDown, player.faceUp )

                            _ ->
                                ( player.faceDown, player.faceUp )
                 in
                 [ if List.isEmpty faceDown then
                    text ""

                   else
                    div
                        [ css
                            [ marginLeft (rem 0.5)
                            , marginRight (rem 0.5)
                            , displayFlex
                            ]
                        ]
                        (faceDown
                            |> List.map
                                (\card ->
                                    div
                                        [ css
                                            [ pseudoClass "not(:nth-child(1))"
                                                [ marginLeft (Css.em -0.33333)
                                                ]
                                            ]
                                        ]
                                        [ cardView i FaceDown card ]
                                )
                        )
                 , if List.isEmpty faceUp then
                    text ""

                   else
                    div
                        [ css
                            [ marginLeft (rem 0.5)
                            , marginRight (rem 0.5)
                            , displayFlex
                            ]
                        ]
                        (faceUp
                            |> List.map
                                (\card ->
                                    div
                                        [ css
                                            [ pseudoClass "not(:nth-child(1))"
                                                [ marginLeft (Css.em -0.33333)
                                                ]
                                            ]
                                        ]
                                        [ if isUser then
                                            cardButton i
                                                [ Attr.disabled (not (canDiscardOwn state.phase player.id))
                                                , onClick (PerformAction (DiscardOwn card))
                                                ]
                                                FaceUp
                                                card

                                          else
                                            cardView i FaceUp card
                                        ]
                                )
                        )
                 ]
                )
            , case ( state.phase, maybePlayerId ) of
                ( FlipOwn { playerId }, _ ) ->
                    if isUser && isActivePlayer && playerId == player.id then
                        div
                            [ css
                                [ position absolute
                                , left (pct 50)
                                , bottom (pct 0)
                                , transforms [ translateX (pct -50), translateY (pct 100), translateY (rem 2.5) ]
                                , whiteSpace noWrap
                                ]
                            ]
                            [ button
                                [ Attr.class "primary"
                                , css [ whiteSpace noWrap ]
                                , onClick (PerformAction FlipAll)
                                ]
                                [ text "Flip all" ]
                            ]

                    else
                        text ""

                ( Attempt { playerId }, _ ) ->
                    if
                        (playerId /= player.id)
                            && (maybePlayerId == Just playerId)
                            && not (List.isEmpty player.faceDown)
                    then
                        div
                            [ css
                                [ position absolute
                                , left (pct 50)
                                , bottom (pct 0)
                                , transforms [ translateX (pct -50), translateY (pct 100), translateY (rem 2.5) ]
                                ]
                            ]
                            [ button
                                [ Attr.class "primary"
                                , css [ whiteSpace noWrap ]
                                , onClick (PerformAction (Flip player.id))
                                ]
                                [ text "Flip one" ]
                            ]

                    else
                        text ""

                ( DiscardTarget { targetId, playerId }, Just userPlayerId ) ->
                    if playerId == userPlayerId && player.id == targetId then
                        div
                            [ css
                                [ position absolute
                                , left (pct 50)
                                , bottom (pct 0)
                                , transforms [ translateX (pct -50), translateY (pct 100), translateY (rem 2.5) ]
                                ]
                            ]
                            [ button
                                [ Attr.class "primary"
                                , css [ Css.whiteSpace noWrap ]
                                , onClick (PerformAction Discard)
                                ]
                                [ text "Discard one" ]
                            ]

                    else
                        text ""

                _ ->
                    text ""
            ]
        ]


handView : Maybe PlayerId -> State -> Index -> Player -> Html Msg
handView maybePlayerId state i player =
    let
        isUser =
            maybePlayerId == Just player.id

        playerCanAddCard =
            canAddCard state.phase player.id

        ( moneys, skulls ) =
            case state.phase of
                DiscardTarget { playerId, targetId } ->
                    if player.id == targetId then
                        ( 0, 0 )

                    else
                        player.hand

                _ ->
                    player.hand

        cardFontSize =
            if isUser then
                Css.rem 2.5

            else
                Css.rem 2

        cardTop =
            if isUser then
                Css.rem 0.5

            else
                Css.rem 1.25
    in
    div
        [ css
            [ displayFlex
            , justifyContent center
            , fontSize cardFontSize
            , position absolute
            , top cardTop
            , left (pct 50)
            , transforms [ translateX (pct -50), translateY (pct -100) ]
            ]
        ]
        (List.concat
            [ List.range 1 skulls
                |> List.map
                    (\_ ->
                        div
                            [ css
                                [ pseudoClass "not(:nth-child(1))"
                                    [ marginLeft (Css.em -0.33333)
                                    ]
                                ]
                            ]
                            (case ( isUser, state.phase ) of
                                ( True, DiscardSelf _ ) ->
                                    [ cardButton i [ onClick (PerformAction (DiscardOwn Skull)) ] FaceUp Skull ]

                                ( True, _ ) ->
                                    [ cardButton i
                                        [ Attr.disabled (not playerCanAddCard)
                                        , onClick (PerformAction (Add Skull))
                                        ]
                                        FaceUp
                                        Skull
                                    ]

                                _ ->
                                    [ cardView i FaceDown Skull ]
                            )
                    )
            , List.range 1 moneys
                |> List.map
                    (\_ ->
                        div
                            [ css
                                [ pseudoClass "not(:nth-child(1))"
                                    [ marginLeft (Css.em -0.3)
                                    ]
                                ]
                            ]
                            (case ( isUser, state.phase ) of
                                ( True, DiscardSelf _ ) ->
                                    [ cardButton i
                                        [ onClick (PerformAction (DiscardOwn Money))
                                        ]
                                        FaceUp
                                        Money
                                    ]

                                ( True, _ ) ->
                                    [ cardButton i
                                        [ Attr.disabled (not playerCanAddCard)
                                        , onClick (PerformAction (Add Money))
                                        ]
                                        FaceUp
                                        Money
                                    ]

                                _ ->
                                    [ cardView i FaceDown Money ]
                            )
                    )
            ]
        )


orderPlayers : Maybe PlayerId -> List Player -> List Player
orderPlayers maybePlayerId players =
    case maybePlayerId of
        Nothing ->
            players

        Just userPlayerId ->
            case splitWhen (\plr -> plr.id == userPlayerId) players of
                Just ( before, user :: after ) ->
                    user :: (after ++ before)

                Just ( _, [] ) ->
                    players

                Nothing ->
                    players
