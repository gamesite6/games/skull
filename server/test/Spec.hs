{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.Set as Set
import Json ()
import Lib
import PlayerId
import Settings

main :: IO ()
main = do
  state <- initialState defaultSettings (Set.fromList (PlayerId <$> [1, 2, 3, 4]))
  BS.putStrLn (encode (toJSON defaultSettings))
  BS.putStrLn (encode (toJSON state))
