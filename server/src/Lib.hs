{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lib
  ( initialState,
    performAction,
    isCompleted,
    playerCounts,
  )
where

import Action (Action)
import qualified Action
import Control.Monad.Random (uniform)
import Control.Monad.Random.Class (MonadRandom)
import Data.Function ((&))
import Data.List (find, findIndex, sort)
import qualified Data.Map as Map
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import PlayerId
import Settings (Settings)
import qualified Settings
import State (State (..))
import qualified State
import State.Card (cardsElem, cardsToList)
import qualified State.Phase as Phase
import State.Phase.Attempt (AttemptPhase (..))
import State.Phase.BidOrPass (BidOrPassPhase (..))
import qualified State.Phase.BidOrPass as BidOrPass
import State.Phase.Discard (DiscardPhase (..))
import State.Phase.FlipOwn (FlipOwnPhase (..))
import State.Phase.Prepare (PreparePhase (..))
import State.Player (Player (..), alive, flippedMoneys, flippedSkull)
import qualified State.Player as Player
import Prelude hiding (id)

playerCounts :: Settings -> [Int]
playerCounts settings =
  let validPlayerCounts = Set.fromList [3, 4, 5, 6]
      selected = Settings.playerCounts settings
      result = Set.intersection validPlayerCounts selected
   in sort $ Set.toList result

initialState :: MonadRandom m => Settings -> [PlayerId] -> m (Maybe State)
initialState settings playerIds =
  if length playerIds `elem` Settings.playerCounts settings
    then
      let players = Player.new <$> playerIds
       in case players of
            firstPlayer : _ ->
              pure $
                Just $
                  State
                    { players = players,
                      phase = Phase.Prepare PreparePhase {active = Set.fromList playerIds, startingPlayerId = Player.id firstPlayer},
                      speech = Map.empty
                    }
            [] -> pure Nothing
    else pure Nothing

getPlayer :: PlayerId -> State -> Maybe Player
getPlayer playerId state = find (\player -> Player.id player == playerId) (State.players state)

getPlayerIndex :: PlayerId -> State -> Maybe Int
getPlayerIndex playerId state = findIndex (\player -> Player.id player == playerId) (State.players state)

adjust :: (a -> a) -> Int -> [a] -> [a]
adjust f idx as =
  case splitAt idx as of
    (_, []) -> as
    (heads, a : tails) -> heads <> (f a : tails)

actionIsAllowed :: State -> Action -> PlayerId -> Bool
actionIsAllowed state@State {players} action performedBy =
  case ( do
           player <- getPlayer performedBy state
           playerIndex <- getPlayerIndex performedBy state
           pure (player, playerIndex)
       ) of
    Nothing -> False
    Just (player, _playerIdx) ->
      case (State.phase state, action) of
        (Phase.Prepare PreparePhase {active}, Action.Add cardToPrepare) ->
          Set.member performedBy active && null (Player.faceDown player) && cardsElem cardToPrepare (Player.hand player)
        (Phase.Prepare PreparePhase {}, _) -> False
        (Phase.AddOrChallenge turnPlayer, Action.Add card) ->
          turnPlayer == performedBy && cardsElem card (Player.hand player)
        (Phase.AddOrChallenge turnPlayer, Action.Bid bid) ->
          turnPlayer == performedBy && bid > 0 && bid <= State.totalPlayedCards players
        (Phase.AddOrChallenge _, _) -> False
        (Phase.BidOrPass BidOrPassPhase {highestBid, playerId, passed}, Action.Bid bid) ->
          playerId == performedBy
            && bid > highestBid
            && (bid <= State.totalPlayedCards players)
            && Set.notMember performedBy passed
        (Phase.BidOrPass BidOrPassPhase {playerId}, Action.Pass) -> performedBy == playerId
        (Phase.BidOrPass _, _) -> False
        (Phase.FlipOwn FlipOwnPhase {playerId}, Action.FlipAll) ->
          performedBy == playerId
        (Phase.FlipOwn _, _) -> False
        (Phase.Attempt AttemptPhase {playerId}, Action.Flip _target) -> performedBy == playerId
        (Phase.Attempt _, _) -> False
        (Phase.DiscardSelf playerId, Action.DiscardOwn card) ->
          performedBy == playerId
            && cardsElem card (Player.allCards player)
        (Phase.DiscardSelf _, _) -> False
        (Phase.DiscardTarget DiscardPhase {playerId}, Action.Discard) ->
          performedBy == playerId
        (Phase.DiscardTarget _, _) -> False
        (Phase.Success playerId, Action.SuccessOk) -> performedBy == playerId
        (Phase.Success _, _) -> False
        (Phase.GameComplete, _) -> False

performAction :: MonadRandom m => Action -> PlayerId -> State -> m (Maybe State)
performAction action activePlayerId state@State {phase, players} =
  if not (actionIsAllowed state action activePlayerId)
    then pure Nothing
    else
      let player : otherPlayers = orderPlayers activePlayerId players
          activePlayerIndex = fromJust $ getPlayerIndex activePlayerId state
       in case (phase, action) of
            (Phase.Prepare prepPhase@PreparePhase {active, startingPlayerId}, Action.Add cardToPrepare) ->
              let nextActive = Set.delete (Player.id player) active
               in pure $
                    Just
                      state
                        { players = adjust (Player.playCard cardToPrepare) activePlayerIndex players,
                          phase =
                            if Set.null nextActive
                              then Phase.AddOrChallenge startingPlayerId
                              else Phase.Prepare prepPhase {active = nextActive}
                        }
            (Phase.Prepare PreparePhase {}, _) -> pure Nothing
            (Phase.AddOrChallenge _, Action.Add discToAdd) ->
              pure $
                let nextPlayer :: PlayerId
                    nextPlayer =
                      otherPlayers
                        & filter alive
                        & head
                        & Player.id
                 in Just
                      state
                        { players = adjust (Player.playCard discToAdd) activePlayerIndex players,
                          phase = Phase.AddOrChallenge nextPlayer
                        }
            (Phase.AddOrChallenge _, Action.Bid bid) ->
              pure $
                let nextPlayer :: PlayerId
                    nextPlayer =
                      otherPlayers
                        & filter alive
                        & head
                        & Player.id
                 in Just
                      state
                        { phase =
                            Phase.BidOrPass
                              BidOrPassPhase
                                { highestBidder = activePlayerId,
                                  highestBid = bid,
                                  playerId = nextPlayer,
                                  passed = Set.empty
                                }
                        }
            (Phase.AddOrChallenge _, _) -> pure Nothing
            (Phase.BidOrPass bidOrPassPhase, Action.Bid bid) ->
              let BidOrPassPhase {passed} = bidOrPassPhase
                  nextPlayer :: PlayerId
                  nextPlayer =
                    otherPlayers
                      & filter alive
                      & filter (\p -> Set.notMember (Player.id p) passed)
                      & head
                      & Player.id
               in pure $
                    Just
                      state
                        { phase =
                            Phase.BidOrPass
                              bidOrPassPhase
                                { highestBidder = activePlayerId,
                                  highestBid = bid,
                                  BidOrPass.playerId = nextPlayer
                                }
                        }
            (Phase.BidOrPass bidOrPassPhase, Action.Pass) ->
              let BidOrPassPhase {passed, highestBid, highestBidder} = bidOrPassPhase
                  nextPassed = Set.insert activePlayerId passed
                  playersStillIn =
                    otherPlayers
                      & filter alive
                      & filter (\p -> Set.notMember (Player.id p) passed)
                  nextPlayerId = Player.id $ head playersStillIn
                  bidPhaseComplete = length playersStillIn == 1
               in pure $
                    Just
                      state
                        { phase =
                            if bidPhaseComplete
                              then Phase.FlipOwn FlipOwnPhase {playerId = highestBidder, bid = highestBid}
                              else
                                Phase.BidOrPass
                                  bidOrPassPhase {BidOrPass.playerId = nextPlayerId, passed = nextPassed}
                        }
            (Phase.BidOrPass _, _) -> pure Nothing
            (Phase.FlipOwn State.Phase.FlipOwn.FlipOwnPhase {bid}, Action.FlipAll) ->
              pure $
                let adjustedPlayer = Player.flipAllFaceDown player
                    nextPlayers = adjust (const adjustedPlayer) activePlayerIndex (State.players state)
                 in Just
                      state
                        { players = nextPlayers,
                          phase = case (flippedSkull adjustedPlayer, flippedMoneys adjustedPlayer >= bid) of
                            (True, _) -> Phase.DiscardSelf activePlayerId
                            (_, True) -> Phase.Success activePlayerId
                            (False, False) -> Phase.Attempt AttemptPhase {playerId = activePlayerId, goal = bid}
                        }
            (Phase.FlipOwn _, _) -> pure Nothing
            (Phase.Attempt AttemptPhase {goal}, Action.Flip target) ->
              pure $ do
                targetPlayer <- getPlayer target state
                targetIndex <- getPlayerIndex target state
                adjustedTarget <- Player.flipTopCard targetPlayer
                let nextPlayers = adjust (const adjustedTarget) targetIndex players
                pure $
                  state
                    { players = nextPlayers,
                      phase =
                        if State.anyFlippedSkull nextPlayers
                          then Phase.DiscardTarget DiscardPhase {playerId = target, targetId = activePlayerId}
                          else
                            if State.totalflippedMoneys nextPlayers >= goal
                              then Phase.Success activePlayerId
                              else phase
                    }
            (Phase.Attempt _, _) -> pure Nothing
            (Phase.DiscardSelf _, Action.DiscardOwn card) ->
              let adjustedPlayers = adjust (Player.discard card) activePlayerIndex $ fmap Player.resetHand players
                  alivePlayers = filter alive adjustedPlayers
               in pure $
                    Just
                      state
                        { players = adjustedPlayers,
                          phase = case alivePlayers of
                            [_] -> Phase.GameComplete
                            _ ->
                              Phase.Prepare
                                PreparePhase
                                  { active = Set.fromList (Player.id <$> alivePlayers),
                                    startingPlayerId = activePlayerId
                                  }
                        }
            (Phase.DiscardSelf _, _) -> pure Nothing
            (Phase.DiscardTarget DiscardPhase {targetId}, Action.Discard) ->
              let adjustedPlayers = fmap Player.resetHand players
                  maybeTargetPlayer = find (\p -> Player.id p == targetId) adjustedPlayers
                  maybeTargetIndex = findIndex (\p -> Player.id p == targetId) adjustedPlayers
               in case (maybeTargetPlayer, maybeTargetIndex) of
                    (Just targetPlayer, Just targetIndex) -> do
                      discardedCard <- uniform (cardsToList (Player.hand targetPlayer))
                      let adjustedTargetPlayer = Player.discard discardedCard targetPlayer
                          adjustedPlayers' = adjust (const adjustedTargetPlayer) targetIndex adjustedPlayers
                          alivePlayers = filter alive adjustedPlayers'
                      pure $
                        Just
                          state
                            { players = adjustedPlayers',
                              phase = case alivePlayers of
                                [_] -> Phase.GameComplete
                                _ ->
                                  Phase.Prepare
                                    PreparePhase
                                      { active = Set.fromList (Player.id <$> alivePlayers),
                                        startingPlayerId = activePlayerId
                                      }
                            }
                    _ -> pure Nothing
            (Phase.DiscardTarget _, _) -> pure Nothing
            (Phase.Success _, Action.SuccessOk) ->
              let adjustedPlayers =
                    players
                      & map Player.resetHand
                      & adjust Player.incrementScore activePlayerIndex
                  alivePlayers = filter alive adjustedPlayers
               in pure $
                    Just
                      state
                        { players = adjustedPlayers,
                          phase =
                            if any (\p -> Player.score p == 2) adjustedPlayers
                              then Phase.GameComplete
                              else
                                Phase.Prepare
                                  PreparePhase
                                    { active = Set.fromList (Player.id <$> alivePlayers),
                                      startingPlayerId = activePlayerId
                                    }
                        }
            (Phase.Success _, _) -> pure Nothing
            (Phase.GameComplete, _) -> pure Nothing

orderPlayers :: PlayerId -> [Player] -> [Player]
orderPlayers playerId players =
  let maybeIdx = findIndex (\p -> Player.id p == playerId) players
   in case maybeIdx of
        Just idx ->
          let (before, player : after) = splitAt idx players
           in player : after <> before
        Nothing -> players

isCompleted :: State -> Bool
isCompleted State {phase} = case phase of
  Phase.GameComplete -> True
  _ -> False
