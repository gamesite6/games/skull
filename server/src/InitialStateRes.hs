module InitialStateRes where

import State (State)

newtype InitialStateRes
  = InitialStateRes
      { state :: State
      }
  deriving (Eq, Show)
