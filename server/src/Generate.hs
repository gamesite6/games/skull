module Generate where

import Action
import Data.Proxy
import Elm.Module
import Json ()
import PlayerId
import Settings
import State
import State.Card
import State.Phase
import State.Phase.Attempt
import State.Phase.BidOrPass
import State.Phase.Discard
import State.Phase.FlipOwn
import State.Phase.Prepare
import State.Player
import State.Speech

main :: IO ()
main =
  putStrLn $
    makeElmModule
      "Gen.Server"
      [ DefineElm (Proxy :: Proxy Action),
        DefineElm (Proxy :: Proxy State),
        DefineElm (Proxy :: Proxy Speech),
        DefineElm (Proxy :: Proxy Settings),
        DefineElm (Proxy :: Proxy Player),
        DefineElm (Proxy :: Proxy Phase),
        DefineElm (Proxy :: Proxy PlayerId),
        DefineElm (Proxy :: Proxy Card),
        DefineElm (Proxy :: Proxy Cards),
        DefineElm (Proxy :: Proxy PreparePhase),
        DefineElm (Proxy :: Proxy BidOrPassPhase),
        DefineElm (Proxy :: Proxy FlipOwnPhase),
        DefineElm (Proxy :: Proxy AttemptPhase),
        DefineElm (Proxy :: Proxy DiscardPhase)
      ]
