module InfoRes where

newtype InfoRes = InfoRes
  { playerCounts :: [Int]
  }
  deriving (Eq, Show)
