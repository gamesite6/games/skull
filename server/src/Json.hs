{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Json where

import Action
import qualified Data.Aeson.TH as A
import Data.Aeson.Types (FromJSONKey, ToJSONKey)
import Elm.Derive (defaultOptions, deriveBoth)
import InfoReq
import InfoRes
import InitialStateReq
import InitialStateRes
import JsonOptions
import PerformActionReq
import PlayerId
import Settings
import State
import State.Card
import State.Phase
import State.Phase.Attempt
import State.Phase.BidOrPass
import State.Phase.Discard
import State.Phase.FlipOwn
import State.Phase.Prepare
import State.Player
import State.Speech

instance ToJSONKey PlayerId

instance FromJSONKey PlayerId

deriveBoth defaultOptions ''PlayerId

deriveBoth defaultOptions ''Speech

deriveBoth defaultOptions ''Action

deriveBoth defaultOptions ''State

deriveBoth (defaultOptions {A.unwrapUnaryRecords = False}) ''Settings

deriveBoth defaultOptions ''Player

deriveBoth defaultOptions ''Phase

deriveBoth defaultOptions {A.constructorTagModifier = renameCard} ''Card

deriveBoth defaultOptions ''Cards

deriveBoth defaultOptions ''PreparePhase

deriveBoth defaultOptions ''BidOrPassPhase

deriveBoth defaultOptions ''FlipOwnPhase

deriveBoth defaultOptions ''AttemptPhase

deriveBoth defaultOptions ''DiscardPhase

deriveBoth (A.defaultOptions {A.unwrapUnaryRecords = False}) ''InfoReq

deriveBoth (A.defaultOptions {A.unwrapUnaryRecords = False}) ''InfoRes

deriveBoth (A.defaultOptions {A.unwrapUnaryRecords = False}) ''InitialStateReq

deriveBoth (A.defaultOptions {A.unwrapUnaryRecords = False}) ''InitialStateRes

deriveBoth (A.defaultOptions {A.unwrapUnaryRecords = False}) ''PerformActionReq

data PerformActionRes = PerformActionRes
  { completed :: Bool,
    nextState :: State
  }
  deriving (Eq, Show)

deriveBoth (A.defaultOptions {A.unwrapUnaryRecords = False}) ''PerformActionRes
