module Action where

import PlayerId
import State.Card

data Action
  = Add Card
  | Bid Int
  | Pass
  | FlipAll
  | Flip PlayerId
  | DiscardOwn Card
  | Discard
  | SuccessOk
  deriving (Eq, Show)
