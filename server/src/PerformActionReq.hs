module PerformActionReq where

import Action (Action)
import PlayerId (PlayerId)
import State (State)

data PerformActionReq
  = PerformActionReq
      { performedBy :: PlayerId,
        action :: Action,
        state :: State,
        seed :: Int
      }
  deriving (Eq, Show)
