module PlayerId where

newtype PlayerId
  = PlayerId Int
  deriving (Eq, Ord, Show)
