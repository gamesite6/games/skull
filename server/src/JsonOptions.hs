module JsonOptions where

import qualified Data.Aeson.TH as A

opts :: A.Options
opts = A.defaultOptions {A.unwrapUnaryRecords = False}

renameCard :: String -> String
renameCard "Skull" = "x"
renameCard "Money" = "o"
renameCard other = other