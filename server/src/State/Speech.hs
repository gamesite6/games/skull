module State.Speech where

data Speech
  = RaisedBid Int
  | Passed
  deriving (Eq, Show)
