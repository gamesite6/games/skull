module State.Card where

import Data.Foldable (foldl')

data Card
  = Money
  | Skull
  deriving (Eq, Show)

newtype Cards = Cards (Int, Int) deriving (Eq, Show)

newCards :: Cards
newCards = Cards (3, 1)

moneys :: Cards -> Int
moneys (Cards (a, _)) = a

skulls :: Cards -> Int
skulls (Cards (_, b)) = b

cardsToList :: Cards -> [Card]
cardsToList (Cards (moneys, skulls)) = replicate moneys Money <> replicate skulls Skull

cardsEmpty :: Cards -> Bool
cardsEmpty cards = cardsSize cards == 0

cardsSize :: Cards -> Int
cardsSize (Cards (moneys, skulls)) = moneys + skulls

cardsElem :: Card -> Cards -> Bool
cardsElem Money cards = moneys cards > 0
cardsElem Skull cards = skulls cards > 0

decrement :: Card -> Cards -> Cards
decrement Skull (Cards (moneys, skulls)) = Cards (moneys, skulls - 1)
decrement Money (Cards (moneys, skulls)) = Cards (moneys - 1, skulls)

increment :: Card -> Cards -> Cards
increment Skull (Cards (moneys, skulls)) = Cards (moneys, skulls + 1)
increment Money (Cards (moneys, skulls)) = Cards (moneys + 1, skulls)

addAll :: Foldable f => f Card -> Cards -> Cards
addAll fa cards = foldl' (flip increment) cards fa
