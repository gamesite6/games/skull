module State.Phase where

import PlayerId
import State.Phase.Attempt (AttemptPhase)
import State.Phase.BidOrPass (BidOrPassPhase)
import State.Phase.Discard (DiscardPhase)
import State.Phase.FlipOwn (FlipOwnPhase)
import State.Phase.Prepare (PreparePhase)

data Phase
  = Prepare PreparePhase
  | AddOrChallenge PlayerId
  | BidOrPass BidOrPassPhase
  | FlipOwn FlipOwnPhase
  | Attempt AttemptPhase
  | DiscardSelf PlayerId
  | DiscardTarget DiscardPhase
  | Success PlayerId
  | GameComplete
  deriving (Eq, Show)
