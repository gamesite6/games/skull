module State.Phase.Discard where

import PlayerId

data DiscardPhase
  = DiscardPhase
      { playerId :: PlayerId,
        targetId :: PlayerId
      }
  deriving (Eq, Show)
