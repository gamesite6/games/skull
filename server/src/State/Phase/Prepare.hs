{-# LANGUAGE NamedFieldPuns #-}

module State.Phase.Prepare where

import Data.Set (Set)
import PlayerId

data PreparePhase
  = PreparePhase
      { active :: Set PlayerId,
        startingPlayerId :: PlayerId
      }
  deriving (Eq, Show)
