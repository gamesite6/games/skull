module State.Phase.Attempt where

import PlayerId

data AttemptPhase
  = AttemptPhase
      { playerId :: PlayerId,
        goal :: Int
      }
  deriving (Eq, Show)
