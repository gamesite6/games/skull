module State.Phase.BidOrPass where

import Data.Set (Set)
import PlayerId

data BidOrPassPhase
  = BidOrPassPhase
      { highestBid :: Int,
        highestBidder :: PlayerId,
        playerId :: PlayerId,
        passed :: Set PlayerId
      }
  deriving (Eq, Show)
