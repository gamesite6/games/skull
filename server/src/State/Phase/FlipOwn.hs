module State.Phase.FlipOwn where

import PlayerId

data FlipOwnPhase
  = FlipOwnPhase
      { playerId :: PlayerId,
        bid :: Int
      }
  deriving (Eq, Show)
