{-# LANGUAGE NamedFieldPuns #-}

module State.Player where

import PlayerId
import State.Card (Card (..), Cards (..), addAll, cardsEmpty, decrement, newCards)
import Prelude hiding (id)

data Player = Player
  { id :: PlayerId,
    hand :: Cards,
    faceDown :: [Card],
    faceUp :: [Card],
    score :: Int
  }
  deriving (Eq, Show)

flippedMoneys :: Player -> Int
flippedMoneys Player {faceUp} = length $ filter (== Money) faceUp

flippedSkull :: Player -> Bool
flippedSkull Player {faceUp} = Skull `elem` faceUp

flipAllFaceDown :: Player -> Player
flipAllFaceDown player@Player {faceDown, faceUp} =
  player
    { faceDown = [],
      faceUp = faceDown <> faceUp
    }

flipTopCard :: Player -> Maybe Player
flipTopCard player@Player {faceDown, faceUp} =
  case faceDown of
    topCard : remaining ->
      Just
        player
          { faceDown = remaining,
            faceUp = topCard : faceUp
          }
    [] -> Nothing

alive :: Player -> Bool
alive = not . dead

dead :: Player -> Bool
dead Player {hand, faceDown, faceUp} = cardsEmpty hand && null faceDown && null faceUp

new :: PlayerId -> Player
new playerId = Player {id = playerId, hand = newCards, faceDown = [], faceUp = [], score = 0}

playCard :: Card -> Player -> Player
playCard card player@Player {hand, faceDown} =
  let nextHand = decrement card hand
      nextFaceDown = card : faceDown
   in player {hand = nextHand, faceDown = nextFaceDown}

playedCards :: Player -> Int
playedCards Player {faceDown, faceUp} = length faceDown + length faceUp

allCards :: Player -> Cards
allCards Player {hand, faceUp, faceDown} = addAll faceUp $ addAll faceDown hand

resetHand :: Player -> Player
resetHand player@Player {hand, faceDown, faceUp} =
  player
    { hand = addAll faceUp $ addAll faceDown hand,
      faceDown = [],
      faceUp = []
    }

discard :: Card -> Player -> Player
discard card player@Player {hand} =
  player
    { hand = decrement card hand
    }

incrementScore :: Player -> Player
incrementScore player@Player {score} = player {score = score + 1}
