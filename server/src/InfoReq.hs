module InfoReq where

import Settings (Settings)

newtype InfoReq
  = InfoReq
      { settings :: Settings
      }
  deriving (Eq, Show)
