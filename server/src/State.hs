{-# LANGUAGE NamedFieldPuns #-}

module State where

import Data.Map (Map)
import PlayerId
import State.Phase
import State.Player (Player)
import qualified State.Player as Player
import State.Speech

data State = State
  { players :: [Player],
    phase :: Phase,
    speech :: Map PlayerId Speech
  }
  deriving (Eq, Show)

totalPlayedCards :: [Player] -> Int
totalPlayedCards = sum . map Player.playedCards

totalflippedMoneys :: [Player] -> Int
totalflippedMoneys = sum . map Player.flippedMoneys

anyFlippedSkull :: [Player] -> Bool
anyFlippedSkull = any Player.flippedSkull
