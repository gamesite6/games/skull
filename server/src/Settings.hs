module Settings where

import Data.Set (Set)
import qualified Data.Set as Set

newtype Settings
  = Settings
      { playerCounts :: Set Int
      }
  deriving (Eq, Show)

defaultSettings :: Settings
defaultSettings = Settings (Set.fromList [3, 4, 5, 6])
