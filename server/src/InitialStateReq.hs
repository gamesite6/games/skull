module InitialStateReq where

import PlayerId (PlayerId)
import Settings (Settings)

data InitialStateReq
  = InitialStateReq
      { players :: [PlayerId],
        settings :: Settings,
        seed :: Int
      }
  deriving (Eq, Show)
