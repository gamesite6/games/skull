{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Control.Monad.Random (evalRand)
import Data.Maybe (fromJust)
import InfoReq (InfoReq)
import qualified InfoReq
import InfoRes (InfoRes (InfoRes))
import InitialStateReq
import InitialStateRes (InitialStateRes (InitialStateRes))
import Json
import qualified Lib
import Network.Wai
import Network.Wai.Handler.Warp
import PerformActionReq
import Servant
import System.Environment.Blank (getEnv)
import System.Random (mkStdGen)

main :: IO ()
main = do
  port :: Int <- read . fromJust <$> getEnv "PORT"
  putStrLn $ "Listening on port " <> show port
  run port app

type GameAPI =
  "info" :> ReqBody '[JSON] InfoReq :> Post '[JSON] InfoRes
    :<|> "initial-state" :> ReqBody '[JSON] InitialStateReq :> Post '[JSON] InitialStateRes
    :<|> "perform-action" :> ReqBody '[JSON] PerformActionReq :> Post '[JSON] PerformActionRes

server :: Server GameAPI
server = handleInfo :<|> handleInitialState :<|> handlePerformAction

handleInfo :: InfoReq -> Handler InfoRes
handleInfo req =
  let settings = InfoReq.settings req
      playerCounts = Lib.playerCounts settings
   in pure $ InfoRes playerCounts

handleInitialState :: InitialStateReq -> Handler InitialStateRes
handleInitialState req@InitialStateReq {players, settings} =
  let seed = InitialStateReq.seed req
      stdGen = mkStdGen seed
   in case evalRand (Lib.initialState settings players) stdGen of
        Just state -> pure $ InitialStateRes state
        Nothing -> throwError $ err422 {errBody = "illegal settings and/or players"}

handlePerformAction :: PerformActionReq -> Handler PerformActionRes
handlePerformAction req@PerformActionReq {performedBy, action, state} =
  let seed = PerformActionReq.seed req
      stdGen = mkStdGen seed
   in case evalRand (Lib.performAction action performedBy state) stdGen of
        Just nextState ->
          pure
            PerformActionRes
              { nextState,
                completed = Lib.isCompleted nextState
              }
        Nothing -> throwError $ err400 {errBody = "illegal game action"}

gameAPI :: Proxy GameAPI
gameAPI = Proxy

app :: Application
app = serve gameAPI server
